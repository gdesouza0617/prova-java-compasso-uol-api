package UOL;
import org.hamcrest.Matchers;
import org.junit.Test;
import io.restassured.RestAssured;

import java.io.PipedOutputStream;

import static io.restassured.RestAssured.given;



public class Prova {
    @Test
    public void cpfsSemRestricao() {

        given().log().all()
                .when().get("http://localhost:8080/api/v1/restricoes/07058979350")
                .then().log().all().statusCode(204);
        given().log().all()
                .when().get("http://localhost:8080/api/v1/restricoes/07039584512")
                .then().log().all().statusCode(204);
        given().log().all()
                .when().get("http://localhost:8080/api/v1/restricoes/07039796579")
                .then().log().all().statusCode(204);
        given().log().all()
                .when().get("http://localhost:8080/api/v1/restricoes/26584756321")
                .then().log().all().statusCode(204);
    }

    @Test
    public void cpfComRestricao(){

        given().log().all()
                .when().get("http://localhost:8080/api/v1/restricoes/97093236014")
                .then().log().all().statusCode(200).body("mensagem", Matchers.is("O CPF 97093236014 tem problema"));
        given().log().all()
                .when().get("http://localhost:8080/api/v1/restricoes/60094146012")
                .then().log().all().statusCode(200).body("mensagem", Matchers.is("O CPF 60094146012 tem problema"));;
        given().log().all()
                .when().get("http://localhost:8080/api/v1/restricoes/84809766080")
                .then().log().all().statusCode(200).body("mensagem", Matchers.is("O CPF 84809766080 tem problema"));
        given().log().all()
                .when().get("http://localhost:8080/api/v1/restricoes/62648716050")
                .then().log().all().statusCode(200).body("mensagem", Matchers.is("O CPF 62648716050 tem problema"));
        given().log().all()
                .when().get("http://localhost:8080/api/v1/restricoes/26276298085")
                .then().log().all().statusCode(200).body("mensagem", Matchers.is("O CPF 26276298085 tem problema"));
        given().log().all()
                .when().get("http://localhost:8080/api/v1/restricoes/01317496094")
                .then().log().all().statusCode(200).body("mensagem", Matchers.is("O CPF 01317496094 tem problema"));
        given().log().all()
                .when().get("http://localhost:8080/api/v1/restricoes/55856777050")
                .then().log().all().statusCode(200).body("mensagem", Matchers.is("O CPF 55856777050 tem problema"));
        given().log().all()
                .when().get("http://localhost:8080/api/v1/restricoes/19626829001")
                .then().log().all().statusCode(200).body("mensagem", Matchers.is("O CPF 19626829001 tem problema"));
        given().log().all()
                .when().get("http://localhost:8080/api/v1/restricoes/24094592008")
                .then().log().all().statusCode(200).body("mensagem", Matchers.is("O CPF 24094592008 tem problema"));
        given().log().all()
                .when().get("http://localhost:8080/api/v1/restricoes/58063164083")
                .then().log().all().statusCode(200).body("mensagem", Matchers.is("O CPF 58063164083 tem problema"));
    }

    @Test
    public void simulacaoNovoCadastro(){
        String payload = "{\n" +
                "  \"nome\": \"Fulano\",\n" +
                "  \"cpf\": 97093236000,\n" +
                "  \"email\": \"email@email.com\",\n" +
                "  \"valor\": 1200,\n" +
                "  \"parcelas\": 3,\n" +
                "  \"seguro\": true\n" +
                "}";
        given()
                .header("Content-Type", "application/json")
                .log().all()
                .when()
                .body(payload)
                .post("http://localhost:8080/api/v1/simulacoes")
                .then()
                .log().all()
                .statusCode(201);;
    }

    @Test
    public void SimulacaoCpfDuplicado(){
        String payload = "{\n" +
                "  \"nome\": \"Fulano de Tal\",\n" +
                "  \"cpf\": 97093236015,\n" +
                "  \"email\": \"email@email.com\",\n" +
                "  \"valor\": 1200,\n" +
                "  \"parcelas\": 3,\n" +
                "  \"seguro\": true\n" +
                "}";
        given()
                .header("Content-Type", "application/json")
                .log().all()
        .when()
                .body(payload)
                .post("http://localhost:8080/api/v1/simulacoes")
                .then()
                .log().all()
                .statusCode(409).body("mensagem", Matchers.is("CPF já existente"));;
    }

    @Test
    public void simulacaoCadastroErrado(){
        String payload = "{\n" +
                "  \"nome\": \"Fulano\",\n" +
                "  \"cpf\": 970932354014,\n" +
                "  \"email\": \"email@email.com\",\n" +
                "  \"valor\": 1200,\n" +
                "  \"parcelas\": 48,\n" +
                "  \"seguro\": \n" +
                "}";
        given()
                .header("Content-Type", "application/json")
                .log().all()
                .when()
                .body(payload)
                .post("http://localhost:8080/api/v1/simulacoes")
                .then()
                .log().all()
                .statusCode(400).body("mensagem", Matchers.is(""));
    }

    @Test
    public void alteracaoSimulacao(){
        String payload = "{\n" +
                "  \"nome\": \"Fulano\",\n" +
                "  \"cpf\": 97093236000,\n" +
                "  \"email\": \"email@email.com\",\n" +
                "  \"valor\": 1200,\n" +
                "  \"parcelas\": 70,\n" +
                "  \"seguro\": false\n" +
                "}";
        given()
                .header("Content-Type", "application/json")
                .log().all()
                .when()
                .body(payload)
                .put("http://localhost:8080/api/v1/simulacoes")
                .then()
                .log().all();

    }

    }
